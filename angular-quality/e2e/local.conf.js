var browserstack = require('browserstack-local');

const {SpecReporter} = require('jasmine-spec-reporter');

exports.config = {
  'seleniumAddress': 'http://hub-cloud.browserstack.com/wd/hub',
  baseUrl: 'http://localhost:4200/',

  'commonCapabilities': {
    'browserstack.user': process.env.BROWSERSTACK_NAME,
    'browserstack.key': process.env.BROWSERSTACK_KEY,
    'name': 'Bstack-[Protractor] Parallel Test',
    'browserstack.local': true,
    'realMobile': 'true',
  },
  'multiCapabilities': [
    {
      'os_version': '8.0',
      'device': 'Google Pixel 2',
      'browserName': 'Android',
    }
  ],

// Code to start browserstack local before start of test
  beforeLaunch: function () {
    console.log('Connecting local');
    return new Promise(function (resolve, reject) {
      exports.bs_local = new browserstack.Local();
      exports.bs_local.start({'key': exports.config.commonCapabilities['browserstack.key']}, function (error) {
        if (error) return reject(error);
        console.log('Connected. Now testing...');

        resolve();
      });
    });
  },

// Code to stop browserstack local after end of test
  afterLaunch: function () {
    return new Promise(function (resolve, reject) {
      exports.bs_local.stop(resolve);
    });
  },


  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
  }
};

exports.config.multiCapabilities.forEach(function(caps){
  for(var i in exports.config.commonCapabilities) caps[i] = caps[i] || exports.config.commonCapabilities[i];
});
