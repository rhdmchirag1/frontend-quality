import { Action } from '@ngrx/store';

export enum OrderActionTypes {
  Update = '[Order] Update Orders',
  UpdateSuccess = '[Order] Update Orders Success',
  UpdateFailed = '[Order] Update Orders Failed',
}

export class UpdateOrder implements Action {
  readonly type = OrderActionTypes.Update;
  constructor(public payload: {item: string}) {
  }
}

export class UpdateOrderSuccess implements Action {
  readonly type = OrderActionTypes.UpdateSuccess;

  constructor(public payload: {items: string[]}) {
  }
}

export class UpdateOrderFailed implements Action {
  readonly type = OrderActionTypes.UpdateFailed;
}


export type OrderActions = UpdateOrder
  | UpdateOrderSuccess
  | UpdateOrderFailed;
