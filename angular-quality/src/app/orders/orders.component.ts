import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getOrder, State } from '../store';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  order$: Observable<string[]>;

  constructor(private store: Store<State>) { }

  ngOnInit() {
    this.order$ = this.store.pipe(select(getOrder));
  }

}
